from django.test import SimpleTestCase

class PagesTest(SimpleTestCase):
	def test_home_page(self):
		response = self.client.get('/api/')
		self.assertEqual(response.status_code, 200)


	def test_about_page(self):
		response = self.client.get('/api/about/')
		self.assertEqual(response.status_code, 200)
